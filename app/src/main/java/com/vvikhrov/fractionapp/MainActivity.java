package com.vvikhrov.fractionapp;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    EditText inputFractionN1, inputFractionN2, inputFractionD1, inputFractionD2;
    TextView outputFractionN;
    TextView outputFractionD;
    TextView inputSign;
    private char sign = '+';

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputFractionN1 = (EditText) findViewById(R.id.et_nominator1);
        inputFractionD1 = (EditText) findViewById(R.id.et_denominator1);

        inputFractionN2 = (EditText) findViewById(R.id.et_nominator2);
        inputFractionD2 = (EditText) findViewById(R.id.et_denominator2);

        outputFractionN = (TextView) findViewById(R.id.output_fraction_nominator);
        outputFractionD = (TextView) findViewById(R.id.output_fraction_denominator);


        inputSign = (TextView) findViewById(R.id.input_sign);
    }


    public void solveExpression(View view) {

        String nom1 = inputFractionN1.getText().toString();
        String den1 = inputFractionD1.getText().toString();

        String nom2 = inputFractionN2.getText().toString();
        String den2 = inputFractionD2.getText().toString();

        if (nom1.isEmpty() || nom2.isEmpty() || den1.isEmpty() || den2.isEmpty()) {
            return;
        }

        Fraction f1 = new Fraction(Integer.valueOf(nom1), Integer.valueOf(den1));
        Fraction f2 = new Fraction(Integer.valueOf(nom2), Integer.valueOf(den2));

        Fraction res = null;

        sign = inputSign.getText().toString().charAt(0);

        switch (sign) {
            case '+' :
                res = f1.add(f2);
                break;
            case '-' :
                res = f1.sub(f2);
                break;
            case '*' :
                res = f1.mult(f2);
                break;
            case '/' :
                res = f1.div(f2);
                break;
        }

        if (res != null) {
            outputFractionN.setText(String.valueOf(res.getNominator()));
            outputFractionD.setText(String.valueOf(res.getDenominator()));
        } else {
            outputFractionN.setText("Err");
            outputFractionD.setText("Err");
        }

    }

    public void plus(View view) {
        inputSign.setText("+");
    }

    public void minus(View view) {
        inputSign.setText("-");
    }

    public void mult(View view) {
        inputSign.setText("*");
    }

    public void div(View view) {
        inputSign.setText("/");
    }
}
