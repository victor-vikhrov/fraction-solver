package com.vvikhrov.fractionapp;

/**
 * Created by victor on 17.10.17.
 */

class Fraction {
    private int nominator;
    private int denominator;

    public Fraction () {
        nominator = 0;
        denominator = 1;
    }

    public Fraction (int n, int d) {
        nominator = n;
        denominator = d;
    }

    public Fraction (Fraction f) {
        nominator = f.nominator;
        denominator = f.denominator;
    }

    public Fraction add (Fraction f) {
        return new Fraction(1,1);
    }


    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public Fraction sub(Fraction f2) {
        return null;
    }

    public Fraction mult(Fraction f2) {
        return null;
    }

    public Fraction div(Fraction f2) {
        return null;
    }
}
